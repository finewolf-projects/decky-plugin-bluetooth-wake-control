# Bluetooth Wake Control

Allows to selectively disable and/or enable Wake-on-Bluetooth
capabilities for your Bluetooth devices.

This is a Decky Plugin version of the ["Wake-on-Bluetooth
Settings"](https://gitlab.com/finewolf-projects/steamos-helpers/-/tree/master/wake-on-bluetooth-settings)
script that does the same thing.

<img src="./docs/assets/Screenshot.png" width="480" alt="Screenshot" />

> **IMPORTANT:**
> 
> The original LCD Deck models do not support Wake-on-Bluetooth
> functionality. This plugin will therefore have no effect on these
> specific models.
>
> Wake-on-Bluetooth hardware support was introduced with the 2023 OLED
> Steam Deck.

## Installation


1.  Install [Decky Loader](https://decky.xyz/)

2.  From the built-in plugin store, install **BT Wake Control**.

## Usage

1.  Open the Quick Access Menu (QAM) by pressing the **…​** button on
    your Steam Deck, or by pressing **Home** + **A** on your controller.

2.  Navigate to the **🔌 Decky** section in the QAM panel.

3.  Click on **BT Wake Control**

    From this screen, you can toggle ON or OFF the ability of specific
    devices to wake your deck.


> **NOTE:**
> 
> Devices that have their toggle disabled do not have the capability of
> waking your Steam Deck. The BlueZ stack therefore does not expose a `WakeAllowed` flag for them.

## Updating

To update the plugin to the latest version, use the built-in Decky
update mechanism.

## How it Works


The BlueZ Linux Bluetooth stack allows end-users to configure a
`WakeAllowed` flag if the device has the capability of waking the host
device on connection.

By default, this is set to `true`, but you can use `dbus` to modify that
value to `false` if that behaviour is not desired.

This plugin interacts with `dbus` and `bluetoothctl` in order to
simplify the management of that internal setting.

