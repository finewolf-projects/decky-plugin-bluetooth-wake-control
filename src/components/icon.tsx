import { type FC, type SVGAttributes, Fragment } from 'react';
import { IconsModule } from 'decky-frontend-lib';

type IconProps = SVGAttributes<SVGElement> & {
  name: string;
};

export const Icon: FC<IconProps> = ({ name, ...svgProps }) => {
  if (IconsModule[name] === undefined) {
    return <Fragment></Fragment>;
  }

  const IconFromModule = IconsModule[name] as FC<SVGAttributes<SVGElement>>;

  return <IconFromModule {...svgProps} />;
};
