import type { FC, SVGAttributes } from 'react';
import type { BluetoothDevice } from '../services/backend';
import { Icon } from './icon';

type DeviceIconProps = SVGAttributes<SVGElement> & {
  device: BluetoothDevice;
};

export const DeviceIcon: FC<DeviceIconProps> = ({ device, ...svgProps }) => {
  let iconName = 'Bluetooth';

  switch (device.icon) {
    case 'audio-headset':
    case 'audio-headphones':
      iconName = 'Headphones';
      break;
    case 'audio-card':
      iconName = 'Music';
      break;
    case 'computer':
      iconName = 'Display';
      break;
    case 'input-gaming':
      iconName = 'Controller';
      break;
    case 'input-keyboard':
      iconName = 'Keyboard';
      break;
    case 'input-mouse':
    case 'input-tablet':
      iconName = 'Mouse';
      break;
    case 'phone':
      iconName = 'Mobile';
      break;
  }

  return <Icon name={iconName} {...svgProps} />;
};
