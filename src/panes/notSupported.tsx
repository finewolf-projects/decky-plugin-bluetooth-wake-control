import { type FC, Fragment } from 'react';
import { PanelSection, PanelSectionRow } from 'decky-frontend-lib';
import { BluetoothWakeStatus } from '../services/backend';
import { Icon } from '../components/icon';

interface NotSupportedPaneProps {
  btWakeStatus: BluetoothWakeStatus;
}

export const NotSupportedPane: FC<NotSupportedPaneProps> = ({ btWakeStatus }) => {
  return (
    <Fragment>
      <PanelSection>
        <PanelSectionRow>
          <div style={{ fontSize: '110%', padding: '0' }}>
            Steam Deck LCD <Icon name='Information' width='1em' style={{ color: '#ffc82c', transform: 'translateY(2px)' }} />
          </div>
        </PanelSectionRow>
        <PanelSectionRow>
          <div style={{ padding: '16px 0 8px 0' }}>
            The Steam Deck LCD model lack the hardware necessary to support Wake-on-Bluetooth. This plugin is unable to provide any
            additional functionality to your particular model of Steam Deck.
          </div>
        </PanelSectionRow>
        <PanelSectionRow>
          <div style={{ color: '#67707b', fontStyle: 'italic', padding: '0' }}>
            Detected Device: &quot;{btWakeStatus.boardVendor} {btWakeStatus.boardName}&quot;
          </div>
        </PanelSectionRow>
      </PanelSection>
    </Fragment>
  );
};
