import { type FC, useEffect, useState, Fragment, PropsWithChildren, useRef } from 'react';
import { BackendService, BluetoothWakeStatus } from '../services/backend';
import { PanelSection, PanelSectionRow, ToggleField, ButtonItem, type ButtonItemProps, Toaster, staticClasses } from 'decky-frontend-lib';
import { DeviceIcon } from '../components/deviceIcon';
import { NotSupportedPane } from './notSupported';
import { Icon } from '../components/icon';

interface MainPanelProps {
  backend: BackendService;
  toaster: Toaster;
}

const ButtonItemWithChildren = ButtonItem as FC<PropsWithChildren<ButtonItemProps>>;

export const MainPanel: FC<MainPanelProps> = ({ backend, toaster }) => {
  const [isRefreshing, setIsRefreshing] = useState<boolean>(true);
  const [hasOpInProgress, setHasOpInProgress] = useState<boolean>(true);
  const [btWakeStatus, setBTWakeStatus] = useState<BluetoothWakeStatus | undefined>();

  useEffect(() => {
    if (isRefreshing) {
      backend.getStatus().then((status) => {
        setIsRefreshing(false);
        setHasOpInProgress(false);
        setBTWakeStatus(status);
      });
    }
  }, [isRefreshing, btWakeStatus]);

  // Fix to scroll panel to top when back button of panel is focused.
  const deviceListWrapperRef = useRef<HTMLDivElement>(null);
  useEffect(() => {
    const wrapperDiv = deviceListWrapperRef.current;

    const eventHandler = (focusEvent: FocusEvent): void => {
      if (!wrapperDiv) return;

      const newlyFocusedElement = focusEvent.relatedTarget as Element;

      // Check if the newly focused element makes sense
      if (!newlyFocusedElement) return;
      if (newlyFocusedElement.tagName !== 'BUTTON') return;

      // If the focus stayed within the same container, ignore event
      if (wrapperDiv.contains(newlyFocusedElement)) return;

      // Get Tab Content Panel
      const tabContentPanel = newlyFocusedElement.closest(`.${staticClasses.TabGroupPanel}`);

      // Check if within active tab
      if (!tabContentPanel?.parentElement?.matches(`.${staticClasses.ActiveTab}`)) return;

      // Reset scroll position to top
      tabContentPanel.scrollTop = 0;
    };

    wrapperDiv?.addEventListener('focusout', eventHandler);

    return () => wrapperDiv?.removeEventListener('focusout', eventHandler);
  }, [deviceListWrapperRef?.current]);

  if (btWakeStatus?.supported === 'unsupported') {
    return <NotSupportedPane btWakeStatus={btWakeStatus} />;
  }

  return (
    <Fragment>
      <PanelSection>
        <PanelSectionRow>
          <div style={{ padding: '8px 0 0', fontSize: '87.5%' }}>
            Turn <span style={{ textTransform: 'uppercase', color: '#1a9fff', fontWeight: '600' }}>on</span> or{' '}
            <span style={{ textTransform: 'uppercase', color: 'rgba(255,255,255,.15)', fontWeight: '600' }}>off</span> the ability of a
            Bluetooth device to wake up your Steam Deck.{' '}
            <span style={{ textTransform: 'uppercase', color: '#67707b', fontWeight: '600' }}>Disabled</span> devices are unable to wake
            your Deck, and cannot be toggled.
          </div>
        </PanelSectionRow>
      </PanelSection>
      <div ref={deviceListWrapperRef}>
        <PanelSection title='Devices' spinner={isRefreshing}>
          {isRefreshing && btWakeStatus === undefined && <PanelSectionRow>Querying devices...</PanelSectionRow>}
          {btWakeStatus !== undefined && (
            <Fragment>
              {btWakeStatus?.devices?.length === 0 && (
                <PanelSectionRow>
                  <div style={{ padding: '8px 0' }}>
                    You currently have no Bluetooth devices paired to this device. Navigate to{' '}
                    <span style={{ fontWeight: '600', whiteSpace: 'nowrap' }}>
                      <Icon name='Settings' width='1em' style={{ transform: 'translateY(2px)' }} />
                      &nbsp;Settings
                    </span>{' '}
                    ›{' '}
                    <span style={{ fontWeight: '600', whiteSpace: 'nowrap' }}>
                      <Icon name='Bluetooth' width='1em' style={{ transform: 'translateY(2px)' }} />
                      &nbsp;Bluetooth
                    </span>{' '}
                    to pair your first device.
                  </div>
                </PanelSectionRow>
              )}
              {btWakeStatus?.devices?.map((device) => (
                <PanelSectionRow key={device.address}>
                  <ToggleField
                    label={<span style={{ fontWeight: '600' }}>{device.name}</span>}
                    icon={<DeviceIcon device={device} width='1em' />}
                    description={
                      <span style={{ color: device.connected === true ? 'inherit' : '#67707b', textTransform: 'uppercase' }}>
                        {device.connected === true ? 'Connected' : 'Not connected'}
                      </span>
                    }
                    highlightOnFocus={true}
                    disabled={device.wakeAllowed === undefined}
                    checked={device.wakeAllowed === true}
                    onChange={(checked: boolean) => {
                      if (hasOpInProgress) {
                        return;
                      }

                      setHasOpInProgress(true);
                      backend.setDeviceWakeAllowed(device, checked).then((success) => {
                        if (!success) {
                          toaster.toast({
                            title: 'Error',
                            body: (
                              <span>
                                Failed to change <code>WakeAllowed</code> for device{' '}
                                <span style={{ fontWeight: '600' }}>{device.name}</span>.
                              </span>
                            ),
                            icon: <Icon name='Caution' width='1em' />,
                            duration: 8000,
                          });
                        }

                        setIsRefreshing(true);
                      });
                    }}
                  />
                </PanelSectionRow>
              ))}
              <PanelSectionRow>
                <ButtonItemWithChildren
                  disabled={isRefreshing === true}
                  layout='below'
                  onClick={(e: MouseEvent) => {
                    if (hasOpInProgress) {
                      e.preventDefault();
                      return;
                    }

                    setHasOpInProgress(true);
                    setIsRefreshing(true);
                  }}
                >
                  Refresh devices
                </ButtonItemWithChildren>
              </PanelSectionRow>
            </Fragment>
          )}
        </PanelSection>
      </div>
    </Fragment>
  );
};
