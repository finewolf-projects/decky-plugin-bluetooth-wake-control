import { definePlugin, ServerAPI, staticClasses } from 'decky-frontend-lib';

import { Icon } from './components/icon';
import { BackendService } from './services/backend';
import { MainPanel } from './panes/main';

export default definePlugin((serverApi: ServerAPI) => {
  const backend = new BackendService(serverApi);

  return {
    title: <div className={staticClasses.Title}>BT Wake Control</div>,
    content: <MainPanel backend={backend} toaster={serverApi.toaster} />,
    icon: <Icon name='Bluetooth' width='1em' />,
  };
});
