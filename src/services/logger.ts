export class ConsoleLogger {
  private constructor() {
    // Prevent construction
  }

  static log(...args: unknown[]) {
    console.log(
      `%c BluetoothWakeCtrl %c INFO %c`,
      'background: #EBD9B4; color: black;',
      'background: #1ABC9C; color: black;',
      'background: transparent;',
      ...args,
    );
  }

  static warn(...args: unknown[]) {
    console.warn(
      `%c BluetoothWakeCtrl %c WARNING %c`,
      'background: #EBD9B4; color: black;',
      'background: #E3C907; color: black;',
      'background: transparent;',
      ...args,
    );
  }

  static error(...args: unknown[]) {
    console.error(
      `%c BluetoothWakeCtrl %c ERROR %c`,
      'background: #EBD9B4; color: black;',
      'background: #C70808; color: black;',
      'background: transparent;',
      ...args,
    );
  }
}
