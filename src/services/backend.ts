import { ServerAPI } from 'decky-frontend-lib';
import { ConsoleLogger } from './logger';

export class BackendService {
  private readonly serverAPI: ServerAPI;

  public constructor(serverAPI: ServerAPI) {
    this.serverAPI = serverAPI;
  }

  public async getStatus(): Promise<BluetoothWakeStatus> {
    const result = await this.serverAPI.callPluginMethod<GetStatusArgs, BluetoothWakeStatus>('get_status', {});

    if (result.success === true) {
      ConsoleLogger.log('getStatus()', result.result);
      return result.result;
    }

    ConsoleLogger.error('getStatus()', result.result);
    throw new Error(result.result);
  }

  public async setDeviceWakeAllowed(device: BluetoothDevice, wakeAllowed: boolean): Promise<boolean> {
    const result = await this.serverAPI.callPluginMethod<
      SetDeviceWakeAllowedArgs,
      {
        address: string;
        success: boolean;
      }
    >('set_device_wake_allowed', {
      address: device.address,
      wakeAllowed: wakeAllowed,
    });

    if (result.success === true) {
      if (result.result.success) {
        ConsoleLogger.log('setDeviceWakeAllowed()', result.result);
      } else {
        ConsoleLogger.error('setDeviceWakeAllowed()', result.result);
      }

      return result.result.success;
    }

    ConsoleLogger.error('setDeviceWakeAllowed()', result.result);
    throw new Error(result.result);
  }
}

type GetStatusArgs = Record<string, never>;

interface SetDeviceWakeAllowedArgs {
  address: string;
  wakeAllowed: boolean;
}

export interface BluetoothDevice {
  address: string;
  name: string;
  alias?: string;
  icon?: string;
  wakeAllowed?: boolean;
  connected?: boolean;
}

export interface BluetoothWakeStatus {
  supported: string;
  boardName: string;
  boardVendor: string;
  devices: BluetoothDevice[];
}
