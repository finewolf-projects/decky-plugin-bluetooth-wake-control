#!/usr/bin/env bash
#shellcheck disable=SC2034 # This script is intented to be sourced

set -euo pipefail

if [ -z "${IS_CI_INITIALIZED:-}" ]; then
  # If we are not in a CI environment, set CI_PROJECT_DIR
  if [ -z "${CI:-}" ]; then
    echo -e $'\e[1m\e[38;5;208m! \e[33mThis script is intented to be executed within a CI environment.\e[0m ' 1>&2
    echo -e $'\e[1m  \e[33mWhile running this directly MAY work, it is not recommended.\e[0m ' 1>&2
    echo 1>&2
    read -p $'\e[1m\e[92m? \e[39mDo you want to continue [yN]:\e[0m ' -n 1 -r # ]]]]
    echo
    if [[ ! "$REPLY" =~ ^[Yy]$ ]]
    then
      exit 99
    fi

    CI_PROJECT_DIR="$( git rev-parse --show-toplevel )"
    CI_COMMIT_REF_NAME="$( git rev-parse --abbrev-ref HEAD )"
    #shellcheck disable=SC2001 # I cannot use search and replace only, needs a regex search
    CI_COMMIT_REF_SLUG="$( echo "${CI_COMMIT_REF_NAME:0:63}" | sed "s/[^a-zA-Z0-9-]/-/g" )"
    CI_COMMIT_SHA="$( git rev-parse --verify HEAD )"
    CI_COMMIT_SHORT_SHA="${CI_COMMIT_SHA:0:8}"
  fi

  if [ -n "${CI_BOTUSER_TOKEN:-}" ]; then
    glab auth login --hostname "${CI_SERVER_HOST}" --token "${CI_BOTUSER_TOKEN}"
  fi

  export IS_CI_INITIALIZED=yes
fi

ci_output_banner() {
  echo ""

  local _MSG="| $* |"
  #shellcheck disable=SC2001
  local _EDGE="$(echo "${_MSG}" | sed 's/./-/g')"

  echo "$_EDGE"
  echo "$_MSG"
  echo "$_EDGE"

  echo ""
}
