#!/usr/bin/env bash
#shellcheck disable=SC2034 # This script is intented to be sourced

set -euo pipefail

if [ -n "${CI:-}" ] || [ ! -d "${CI_PROJECT_DIR}/node_modules" ]; then
  (cd "${CI_PROJECT_DIR}" && mkdir -p "${CI_PROJECT_DIR}/.pnpm-store" && corepack enable && corepack prepare pnpm@latest-8 --activate && pnpm config set store-dir "${CI_PROJECT_DIR}/.pnpm-store" && pnpm install --frozen-lockfile) || (echo "Failed to install node packages, bailing out." 1>&2 && exit 2)
fi
