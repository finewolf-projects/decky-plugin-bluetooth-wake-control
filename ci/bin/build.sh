#!/usr/bin/env bash
set -euo pipefail
_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)" || (echo "Couldn't determine the script's running directory, which probably matters, bailing out" 1>&2 && exit 2)

#shellcheck source=./../lib/init-environment.sh
source "${_SCRIPT_DIR}/../lib/init-environment.sh"

cd "${CI_PROJECT_DIR}" || (echo "Couldn't navigate to project directory, bailing out." 1>&2 && exit 2)

ci_output_banner "Building TypeScript/React"

#shellcheck source=./../lib/prepare-node-packages.sh
source "${_SCRIPT_DIR}/../lib/prepare-node-packages.sh"

pnpm run build

ci_output_banner "Preparing Package"

# Read information from plugin.json and package.json
PLUGIN_SLUG="$(jq -r .name "${CI_PROJECT_DIR}/package.json")"
PLUGIN_VERSION="$(jq -r .version "${CI_PROJECT_DIR}/package.json")"

# Create staging
STAGING_ROOT="${CI_PROJECT_DIR}/ci/out/.staging"
STAGING_DIR="${STAGING_ROOT}/${PLUGIN_SLUG}"
rm -rf "${STAGING_ROOT}"
mkdir -p "${STAGING_DIR}"

# Prepare rsync includes file
TMP_INCLUDES_FILE="$(mktemp)"
trap 'rm -f -- "${TMP_INCLUDES_FILE}"' EXIT

cat <<EOF >"${TMP_INCLUDES_FILE}"
dist
dist/**
LICENSE
main.py
package.json
plugin.json
README.md
EOF

# Copy files into staging
rsync --archive --include-from="${TMP_INCLUDES_FILE}" --exclude='*' "${CI_PROJECT_DIR}/" "${STAGING_DIR}"

# Handle defaults folder
if [ -d "${CI_PROJECT_DIR}/defaults" ]; then
  rsync --archive "${CI_PROJECT_DIR}/defaults/" "${STAGING_DIR}"
fi

# Apply commit sha to version if not building a tag, and add debug flag.
if [[ "${CI_COMMIT_BRANCH:-}" != "master" ]] && [[ ! "${CI_COMMIT_TAG:-}" =~ ^v[0-9.]+ ]]; then
  PLUGIN_VERSION="${PLUGIN_VERSION}-${CI_COMMIT_SHORT_SHA}"
  jq ".version = \"${PLUGIN_VERSION}\"" "${STAGING_DIR}/package.json" > "${STAGING_DIR}/package.json.updated"
  cp -f "${STAGING_DIR}/package.json.updated" "${STAGING_DIR}/package.json"
  rm -f "${STAGING_DIR}/package.json.updated"

  jq ".flags += [\"debug\"]" "${STAGING_DIR}/plugin.json" > "${STAGING_DIR}/plugin.json.updated"
  cp -f "${STAGING_DIR}/plugin.json.updated" "${STAGING_DIR}/plugin.json"
  rm -f "${STAGING_DIR}/plugin.json.updated"
fi

ci_output_banner "Zipping Package"

PACKAGE_ZIP="${CI_PROJECT_DIR}/ci/out/${PLUGIN_SLUG}_v${PLUGIN_VERSION}.zip"

if [ -f "${PACKAGE_ZIP}" ]; then
  rm -f "${PACKAGE_ZIP}";
fi

(cd "${STAGING_ROOT}" && zip -r "${PACKAGE_ZIP}" -9 .)

rm -rf "${STAGING_ROOT}"

echo ""
echo "Created: ${PACKAGE_ZIP}"
