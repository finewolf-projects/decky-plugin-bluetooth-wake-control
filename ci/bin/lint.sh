#!/usr/bin/env bash
set -euo pipefail
_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)" || (echo "Couldn't determine the script's running directory, which probably matters, bailing out" 1>&2 && exit 2)

#shellcheck source=./../lib/init-environment.sh
source "${_SCRIPT_DIR}/../lib/init-environment.sh"

cd "${CI_PROJECT_DIR}" || (echo "Couldn't navigate to project directory, bailing out." 1>&2 && exit 2)

ci_output_banner "Linting React/TypeScript code..."

#shellcheck source=./../lib/prepare-node-packages.sh
source "${_SCRIPT_DIR}/../lib/prepare-node-packages.sh"

pnpm run lint
