#!/usr/bin/env bash
set -euo pipefail
_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)" || (echo "Couldn't determine the script's running directory, which probably matters, bailing out" 1>&2 && exit 2)

#shellcheck source=./../lib/init-environment.sh
source "${_SCRIPT_DIR}/../lib/init-environment.sh"

cd "${CI_PROJECT_DIR}" || (echo "Couldn't navigate to project directory, bailing out." 1>&2 && exit 2)

if [ -z "${S3_UPLOAD_PREFIX:-}" ]; then
  echo "S3_UPLOAD_PREFIX not specified, bailing out." 1>&2
  exit 1
fi

ci_output_banner "Uploading release"

if [ ! -f "${CI_PROJECT_DIR}/ci/out/release.env" ]; then
  echo "prepare-changelog.sh must be run first." 1>&2
  exit 1
fi

source "${CI_PROJECT_DIR}/ci/out/release.env"

if [ ! -f "${CI_PROJECT_DIR}/ci/out/${RELEASE_ZIPFILE_NAME}" ]; then
  echo "ZipFile ${RELEASE_ZIPFILE_NAME} not found." 1>&2
  exit 1
fi

aws s3 cp "${CI_PROJECT_DIR}/ci/out/${RELEASE_ZIPFILE_NAME}" "${S3_UPLOAD_PREFIX}${RELEASE_ZIPFILE_PATH}"
