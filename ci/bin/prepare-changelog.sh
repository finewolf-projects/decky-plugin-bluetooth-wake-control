#!/usr/bin/env bash
set -euo pipefail
_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)" || (echo "Couldn't determine the script's running directory, which probably matters, bailing out" 1>&2 && exit 2)

#shellcheck source=./../lib/init-environment.sh
source "${_SCRIPT_DIR}/../lib/init-environment.sh"

cd "${CI_PROJECT_DIR}" || (echo "Couldn't navigate to project directory, bailing out." 1>&2 && exit 2)

ci_output_banner "Preparing Changelog"

PLUGIN_ZIP_COUNT="$(find "${CI_PROJECT_DIR}/ci/out/" -maxdepth 1 -type f -name '*.zip' | wc -l)"

if [ "${PLUGIN_ZIP_COUNT}" -ne 1 ]; then
  echo "ASSERT FAILED: Found ${PLUGIN_ZIP_COUNT} zips, expected 1" >&2
  exit 1
fi

PLUGIN_ZIPFILE_PATH="$(find "${CI_PROJECT_DIR}/ci/out/" -maxdepth 1 -type f -name '*.zip')"
PLUGIN_ZIPFILE_PACKAGE_JSON_PATH="$(zipinfo -1 "${PLUGIN_ZIPFILE_PATH}" | grep -E '^[^/]+/package\.json$')"
PLUGIN_ZIPFILE_NAME="$(basename "${PLUGIN_ZIPFILE_PATH}")"

PLUGIN_VERSION="$(unzip -p "${PLUGIN_ZIPFILE_PATH}" "${PLUGIN_ZIPFILE_PACKAGE_JSON_PATH}" | jq -r .version)"

# Ensure version is unique
if git rev-list -n1 "refs/tags/v${PLUGIN_VERSION}" >/dev/null 2>&1; then
  echo "Release v${PLUGIN_VERSION} already exists; please bump 'version' in package.json" >&2
  exit 1
fi

# Detect Previous Version
PREVIOUS_VERSION="$(git tag --sort=-v:refname | (grep -E '^v([0-9]+([.]|$))+$' || true) | head -1 | sed 's#^v##')"
PREVIOUS_VERSION_SHA=""

if [ -n "${PREVIOUS_VERSION}" ]; then
  PREVIOUS_VERSION_SHA="$(git rev-list -n1 "refs/tags/v${PREVIOUS_VERSION}")"
else
  PREVIOUS_VERSION_SHA="$(git rev-list --max-parents=0 -n1 HEAD)"
fi

# Generate Changelog
glab changelog generate --version "v${PLUGIN_VERSION}" --from "${PREVIOUS_VERSION_SHA}" > "${CI_PROJECT_DIR}/ci/out/CHANGELOG.md"

# Generate dotenv
cat <<EOF >"${CI_PROJECT_DIR}/ci/out/release.env"
RELEASE_TAG="v${PLUGIN_VERSION}"
RELEASE_VERSION="${PLUGIN_VERSION}"
RELEASE_ZIPFILE_NAME="${PLUGIN_ZIPFILE_NAME}"
RELEASE_ZIPFILE_PATH="${CI_PROJECT_PATH:-project}/v${PLUGIN_VERSION}/${PLUGIN_ZIPFILE_NAME}"
EOF
