#!/usr/bin/env bash
set -euo pipefail
_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)" || (echo "Couldn't determine the script's running directory, which probably matters, bailing out" 1>&2 && exit 2)

#shellcheck source=./../lib/init-environment.sh
source "${_SCRIPT_DIR}/../lib/init-environment.sh"

cd "${CI_PROJECT_DIR}" || (echo "Couldn't navigate to project directory, bailing out." 1>&2 && exit 2)

ci_output_banner "Version Check"

PLUGIN_VERSION="$(jq -r .version "${CI_PROJECT_DIR}/package.json")"

# Ensure version is unique
if git rev-list -n1 "refs/tags/v${PLUGIN_VERSION}" >/dev/null 2>&1; then
  echo "Release v${PLUGIN_VERSION} already exists; please bump 'version' in package.json" >&2
  exit 1
fi

echo "Release v${PLUGIN_VERSION} is available."

ci_output_banner "Changelog Preview"

# Preview Changelog
mkdir -p "${CI_PROJECT_DIR}/ci/out"

# Detect Previous Version
PREVIOUS_VERSION="$(git tag --sort=-v:refname | (grep -E '^v([0-9]+([.]|$))+$' || true) | head -1 | sed 's#^v##')"
PREVIOUS_VERSION_SHA=""

if [ -n "${PREVIOUS_VERSION}" ]; then
  PREVIOUS_VERSION_SHA="$(git rev-list -n1 "refs/tags/v${PREVIOUS_VERSION}")"
else
  PREVIOUS_VERSION_SHA="$(git rev-list --max-parents=0 -n1 HEAD)"
fi

# Generate Changelog
glab changelog generate --version "v${PLUGIN_VERSION}" --from "${PREVIOUS_VERSION_SHA}" > "${CI_PROJECT_DIR}/ci/out/CHANGELOG.md"

cat "${CI_PROJECT_DIR}/ci/out/CHANGELOG.md"
