import os
import subprocess
import re

class Plugin:
  async def get_status(self):
    # Check if Wake-on-Bluetooth is supported on this device.
    supported = "unknown"
    path_board_vendor = "/sys/devices/virtual/dmi/id/board_vendor"
    path_board_name = "/sys/devices/virtual/dmi/id/board_name"

    board_vendor = "unknown"
    board_name = "unknown"

    try:
      if os.path.isfile(path_board_vendor) and os.access(path_board_vendor, os.R_OK):
        with open(path_board_vendor) as f:
          board_vendor = f.readlines(1)[0].strip()
      
      if os.path.isfile(path_board_name) and os.access(path_board_name, os.R_OK):
        with open(path_board_name) as f:
          board_name = f.readlines(1)[0].strip()
    except OSError:
      board_vendor = "unknown"
      board_name = "unknown"

    if board_vendor == "Valve":
      if board_name == "Jupiter":
        supported = "unsupported"
      else:
        supported = "supported"

    # Read Devices
    devices = []

    btCtlDevices = subprocess.run(["bluetoothctl", "devices", "Paired"], timeout=10, text=True, capture_output=True)
    if btCtlDevices.returncode == 0:
      for btCtlDeviceLine in btCtlDevices.stdout.splitlines():
        btDeviceAddr = re.search(r"^\w+\s+([A-F0-9:]+)", btCtlDeviceLine).group(1)
        btDeviceInfo = subprocess.run(["bluetoothctl", "info", btDeviceAddr], timeout=10, text=True, capture_output=True)
        if btDeviceInfo.returncode != 0:
          continue
        device = {"address": btDeviceAddr}
        for btDeviceInfoLine in btDeviceInfo.stdout.splitlines():
          fieldMatch = re.search(r"^\s+([^\s:]+):\s*(.+)\s*$", btDeviceInfoLine)
          if fieldMatch is None:
            continue
          fieldName = fieldMatch.group(1)
          fieldNameNormalized = fieldName[0].lower() + fieldName[1:]
          fieldValue = fieldMatch.group(2)

          if fieldName == "Name" or fieldName == "Alias" or fieldName == "Icon":
            device[fieldNameNormalized] = fieldValue
          elif fieldName == "WakeAllowed" or fieldName == "Connected":
            device[fieldNameNormalized] = fieldValue == "yes"
        devices.append(device)

    # Sort Devices (Connected devices first, then by name, then by address)
    sortedDevices = sorted(devices, key=self.__sortKeyDevice)

    return {"supported": supported, "boardName": board_name, "boardVendor": board_vendor, "devices": sortedDevices}

  async def set_device_wake_allowed(self, address: str, wakeAllowed: bool):
    dbusAddress = address.replace(":", "_")
    desiredState = "true" if wakeAllowed else "false"

    dbusResult = subprocess.run([
      "dbus-send",
      "--print-reply=literal",
      "--system",
      "--dest=org.bluez",
      "/org/bluez/hci0/dev_"+dbusAddress,
      "org.freedesktop.DBus.Properties.Set",
      "string:org.bluez.Device1",
      "string:WakeAllowed",
      "variant:boolean:"+desiredState
    ], timeout=10, text=True, capture_output=True)

    return {"address": address, "success": dbusResult.returncode == 0}

  # Called when plugin is installed, or loaded on bootup
  async def _main(self):
    pass

  # Called when plugin is uninstalled
  async def _unload(self):
    pass

  # Create a sort key when sorting devices
  def __sortKeyDevice(dev):
    isConnected = "A" if (dev["connected"] or False) else "Z"
    deviceName = dev["name"] or ""
    deviceAddress = dev["address"] or "FF:FF:FF:FF:FF:FF"
    return isConnected + "|" + deviceName + "|" + deviceAddress
