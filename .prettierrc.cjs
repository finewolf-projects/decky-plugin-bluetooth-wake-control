/* eslint-env node, commonjs */
/* eslint-disable @typescript-eslint/no-var-requires */

/** @type {import("@types/prettier").Options */
module.exports = {
  semi: true,
  tabWidth: 2,
  printWidth: 140,
  singleQuote: true,
  jsxSingleQuote: true,
  trailingComma: 'all',
  plugins: [require.resolve('@prettier/plugin-xml')],
  overrides: [
    {
      files: '*.eslintrc',
      options: {
        parser: 'json',
      },
    },
  ],
};
